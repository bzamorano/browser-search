import logging
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as COptions


# Class ChromeDriver configures chromedriver as the only driver executable.
# chromedriver will let us navigate through html elements
class ChromeDriver:
    path = 'C:\\Users\\bzamorano\\Documents\\Drivers\\chromedriver.exe'
    driver = None
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger('SETUP')

    # Chrome is set as a driver option
    def chrome(self):
        chrome_options = COptions()
        chrome_options.add_argument("--headless")
        self.driver = webdriver.Chrome(
            executable_path=self.path, options=chrome_options)

    def get_driver(self):
        return self.driver

    def __init__(self):
        try:
            self.chrome()
            self.logger.info("Web Driver created")
        except Exception as exc:
            self.logger.info("Web driver not available: {0}".format(exc))
