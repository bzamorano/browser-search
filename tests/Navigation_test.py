import logging
import unittest
from poms.SearchResults import SearchResults
from poms.Navigation import Navigation
from drivers.ChromeDriver import ChromeDriver


class Navigation_test(unittest.TestCase):
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(' MAIN TEST ')
    Navigation = None
    SearchResults = None
    driver = None

    @classmethod
    def setUpClass(self) -> None:
        chrome_driver = ChromeDriver()
        self.driver = chrome_driver.get_driver()
        self.Navigation = Navigation(self.driver)
        self.SearchResults = SearchResults(self.driver)
        self.Navigation.search_engine()
        self.Navigation.search_input()
        self.SearchResults.get_search_results()

    def test_check_suggestion(self):
        self.assertIsNotNone(self.SearchResults.suggestions_available(),
                             "Search suggestion ERROR")
        self.SearchResults.get_search_results()

    def test_verify_description(self):
        has_description_in_all_results = self.SearchResults.has_description_in_all_results()
        self.assertFalse(has_description_in_all_results,
                         "Found results with no description")
        self.logger.info("Results with description")

    def test_verify_title(self):
        self.assertTrue(self.SearchResults.results_title(),
                        "Titles contain key word.")

    @classmethod
    def tearDownClass(self) -> None:
        self.driver.close()


if __name__ == '__main__':
    unittest.main()
