import logging
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


# Class SearchResults saves all elements founded in Google's search into a list, which
# will be iterate to make the corresponding validations.
class SearchResults:
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger("RESULTS")
    found_results_count = 0
    driver = None
    results = None
    query = None

    def __init__(self, driver: webdriver):
        self.driver = driver

    # Method get_search_result saves the results "//h3[@class='LC20lb DKV0Md']" in a list and
    # prints the text of each result. If there are not elements, a "Zero results" log is printed.
    def get_search_results(self):
        try:
            wait = WebDriverWait(self.driver, 10)
            self.results = wait.until(
                ec.presence_of_all_elements_located((By.XPATH, "//h3[@class='LC20lb DKV0Md']")))
            self.found_results_count = len(self.results)
            for result in self.results:
                if result.text:
                    print(result.text)
        except NoSuchElementException:
            self.logger.info("Zero results")

    # Method suggestions_available will validate if there are any elements in
    # suggestions in "//a[@class='gL9Hy']", if there are, it will save the result in "query" in click
    # on the suggestions element.
    def suggestions_available(self, iterations=0):
        try:
            wait = WebDriverWait(self.driver, 10)
            suggestion = self.driver.find_element_by_xpath("//a[@class='gL9Hy']")
            self.query = suggestion.text
            suggestion.click()
            click = True
            self.found_results_count = 0
            self.suggestions_available(1)
        # In case there are not suggestions available, a "No suggestions available" logger
        # will be printed.
        except NoSuchElementException:
            click = False
            if iterations == 0:
                self.logger.info("No suggestions available")
        return click

    # Method has_description_in_all_results searches for a description in element "//span[@class='st' and
    # normalize-space( text())]". If found, returns true.
    def has_description_in_all_results(self):
        descriptions = self.driver.find_elements_by_xpath("//span[@class='st' and normalize-space(text())]")
        total_descriptions = len(descriptions)
        # Compares descriptions and results, if the values are equal return true.
        all_have_description = total_descriptions == self.found_results_count
        if not all_have_description:
            self.logger.info("There are elements with no description")
        return all_have_description

    # Method results_title searches for title value in "//input[@class='gLFyf gsfi']", if this value
    # contains the user's input, returns true.
    def results_title(self):
        if self.query is None:
            self.query = self.driver.find_element_by_xpath(
                "//input[@class='gLFyf gsfi']"). \
                get_attribute('value')
        split_query = self.query.split()
        first_result = self.results[0].text.lower()
        for input in split_query:
            # In case the result contains user's input, it returns 
            if input.lower() in first_result:
                self.logger.info("First result contains searched word")
                return True
        return False
