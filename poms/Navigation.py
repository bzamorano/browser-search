import logging
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait


# Class Navigation will navigate through Google search page and search
# for an input query.
class Navigation:
    driver = None
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger('Navigation')

    def __init__(self, driver: webdriver):
        self.driver = driver

    # Method search_engine uses driver's method get to launch Google page
    def search_engine(self):
        self.driver.get("https://www.google.com.mx/")

    # Method search_input searches first searches for the search bar web element("q")
    # After "q" is visible, user's input is saved in variable "query" and sent as key to "q"
    def search_input(self):
        wait = WebDriverWait(self.driver, 5)
        search_input = wait.until(ec.visibility_of_element_located
                                  ((By.NAME, "q")))
        query = input("What do you want to search_input?: ")
        search_input.clear()
        search_input.send_keys(str(query))
        search_input.send_keys(Keys.ENTER)
        self.logger.info("Search completed: '" + query + "'")
